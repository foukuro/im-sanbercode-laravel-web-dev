@extends('layout.master')

@section('title')
    halaman edit cast
@endsection
@section('subtitle')
edit cast
@endsection
@section('content')

<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>nama</label>
        <input type="text" class="form-control" value="{{ $cast->nama }}" name="nama"  placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>umur</label>
        <input type="text" class="form-control" value="{{ $cast->umur }}" name="umur" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>bio</label>
        <input type="text" class="form-control" value="{{ $cast->bio }}" name="bio" placeholder="Masukkan bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection