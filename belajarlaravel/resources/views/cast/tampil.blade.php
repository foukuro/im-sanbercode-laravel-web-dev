@extends('layout.master')

@section('judul')
    halaman list cast
@endsection
@section('judul')
list cast
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">tambah</a>

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col" >action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="/cast/{{ $value->id }}" method="POST">
                        @method('delete')
                        @csrf
                        <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">detail</a>
                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">edit</a>
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>

@endsection