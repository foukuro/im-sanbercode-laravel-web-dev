@extends('layout.master')

@section('title')
    halaman tambah cast
@endsection
@section('subtitle')
tambah cast
@endsection
@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>nama</label>
        <input type="text" class="form-control" name="nama"  placeholder="Masukkan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>umur</label>
        <input type="text" class="form-control" name="umur" placeholder="Masukkan umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>bio</label>
        <input type="text" class="form-control" name="bio" placeholder="Masukkan bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection