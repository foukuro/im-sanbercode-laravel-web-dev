<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'utama']);

route::get('/table', [AuthController::class, 'bio']);

route::post('/submit', [AuthController::class, 'submit']);

route::get('/data-table', function(){
    return view('halaman.data-table');
});


//CRUD

// create data
route::get('/cast/create', [castController::class, 'create']);
route::post('/cast', [castController::class, 'store']);


//read data
route::get('/cast',[castController::class, 'index']);
route::get('/cast/{id}',[castController::class, 'show']);

//update data
route::get('/cast/{id}/edit',[castController::class, 'edit']);
route::put('/cast/{id}',[castController::class, 'update']);

//delete data
route::delete('/cast/{id}',[castController::class, 'destroy']);